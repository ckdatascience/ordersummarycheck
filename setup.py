from setuptools import setup
import setuptools

setup(name='OrderSummaryCheck',
      version='0.1',
      description='Order Summary Check File',
      author='Charles and Keith',
      author_email='vignesh.srinivasan@charleskeith.com',
      license='Charles & Keith',
      packages=setuptools.find_packages(),
      zip_safe=False)