from Rules.Rules import Rules
import pandas as pd

class ExcelWriter(Rules):

    def initialize_writer(self,path):
        writer = pd.ExcelWriter(path)
        return writer

    def write_excel(self,writer,data,sheet_name):
        try:
            data.to_excel(writer,sheet_name)
            return True
        except Exception as e:
            print e
        return False

    def save_writer(self,writer):
        writer.save()
        writer.close()


