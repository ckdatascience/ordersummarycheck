import os


class Rules:
    def set_franchise_list_file(self, franchise_list_data_path):
        self.franchise_list_data = franchise_list_data_path

    def set_ros_file(self, ros_file):
        self.ros_file = ros_file

    def set_bill_to_store_file(self, bill_to_store_file):
        self.bill_to_store = bill_to_store_file

    def set_item_master(self, item_master_file):
        self.item_master = item_master_file

    def set_order_summary_file(self, order_summary_file):
        self.order_summary_file = order_summary_file

    def set_article_list_file(self, article_list_path):
        self.article_list_file = article_list_path

    def set_business_rule_file(self, business_rule_file):
        self.business_rule_file = business_rule_file

    def set_retail_sales_target_path(self,retail_sales_target):
        self.retail_sales_target=retail_sales_target

    def get_retail_sales_target_path(self):
        return self.retail_sales_target

    def get_business_rule_path(self):
        return self.business_rule_file

    def get_article_list_path(self):
        return self.article_list_file

    def get_all_combined_order_summary(self):
        return self.order_summary_file

    def get_item_master_path(self):
        return self.item_master

    def get_bill_to_store_list_path(self):
        return self.bill_to_store

    def get_final_summer_combined_os_path(self):
        return self.final_summer_combined_os_path

    def set_final_order_summary_path(self, final_summer_combined_os_path):
        self.final_summer_combined_os_path=final_summer_combined_os_path

    def get_final_ros_depth_path(self):
        return self.final_ros_depth_path

    def set_final_ros_depth_path(self, final_ros_depth_path):
        self.final_ros_depth_path=final_ros_depth_path

    def get_return_on_sales_path(self):
        return self.ros_file

    def get_franchise_list_path(self):
        return self.franchise_list_data






