import pandas as pd
from Rules.Rules import Rules


class ProductLevelCondition():
    def __init__(self,rules):
        self.rules=rules

    def load_business_rules(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'Condition',header=0,converters={'Cat':str,'Category':str,'Fac':str,'Condition':str})
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df










