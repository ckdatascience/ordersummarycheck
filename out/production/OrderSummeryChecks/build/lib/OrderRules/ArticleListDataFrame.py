import pandas as pd
from  Rules.TestRules import Rules


class ValidateArticle():
    def __init__(self,rules):
        self.rules=rules
    def load_business_rules(self):
        article_list_path=self.rules.get_article_list_path()
        df = pd.read_excel(article_list_path,'ALL',header=0).iloc[:,:2]
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df

    def get_article_list(self,df):
        df = df.drop_duplicates(df.columns.difference(['Final Article Number']))
        return df










