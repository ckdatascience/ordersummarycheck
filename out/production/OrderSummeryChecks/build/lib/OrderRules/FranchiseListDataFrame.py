import pandas as pd
from  Rules.Rules import Rules

class FranchiseListDataFrame():

    def __init__(self,rules):
        self.rules=rules
    def load_franchise_list_path(self):
        business_rule_path=self.rules.get_franchise_list_path()
        df = pd.read_excel(business_rule_path,'Summer 19 FOB by Customer').iloc[:,:9]
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df









