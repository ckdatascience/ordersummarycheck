import pandas as pd
from  Rules.Rules import Rules


class QuantityLevelCondition():

    def __init__(self,rules):
        self.rules=rules

    def load_business_rules(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'Qty',header=0,converters={'Orders As Per':str,'Condition':str})
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df

    def get_article_level_qty_params(self,df):
        return df.loc[df['Orders As Per'].isin(['ARTICLE'])]

    def get_colour_level_qty_params(self,df):
        return df.loc[df['Orders As Per'].isin(['COLOUR'])]

    def is_quantity_approved(self,ordered_qty,tot_quantity,min_order):
        if  ordered_qty==0 and tot_quantity>=min_order:
            return "APPROVED"
        return "REJECTED"



