import pandas as pd
from  Rules.Rules import Rules


class ValidateBilledStore():
    def __init__(self,rules):
        self.rules=rules

    def get_billed_shop_path_list(self):
        ship_to_store_list_path=self.rules.get_bill_to_store_list_path()
        df = pd.read_excel(ship_to_store_list_path,'Ship To-Sold To (Overall)')
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df








