import pandas as pd
from Rules.Rules import Rules


class ValidateArticleOrderLocationType():
    def __init__(self,rules):
        self.rules=rules

    def load_business_rules(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'Loc',header=0,converters={'Condition':str})
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df

    def get_order_location_type(self,df ,type):
        return df.loc[df['Orders Location Types'].isin([type])]





