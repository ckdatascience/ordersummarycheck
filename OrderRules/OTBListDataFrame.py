import pandas as pd
from   Rules.Rules import Rules


class OTBListDataFrame():

    def __init__(self,rules):
        self.rules=rules

    def load_otb_list_path(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'OTB_Header')

        return df


    def load_otb_self_run_countries(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'OTB_SelfRun')

        return df










