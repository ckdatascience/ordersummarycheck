import pandas as pd
from   Rules.Rules import Rules


class ProductWallThemeCondition():
    def __init__(self,rules):
        self.rules=rules

    def get_product_level_theme_condition(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'Group')
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df







