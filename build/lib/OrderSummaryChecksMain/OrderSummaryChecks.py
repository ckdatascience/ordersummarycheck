
from Utils.utils import *

import numpy as np
from OrderRules.ProductLevelCondition import ProductLevelCondition
from OrderRules.QuantityLevelCondition import QuantityLevelCondition
from OrderRules.ArticleListDataFrame import ValidateArticle
from OrderRules.ItemMaster import ItemMaster
from OrderRules.ArticleSizeListDataFrame import ArticleSize
from OrderRules.BilledStoreListDataFrame import ValidateBilledStore
from OrderRules.ArticleOrderLocationTypeDataFrame import ValidateArticleOrderLocationType
from Rules.Rules import Rules
from ExcelWriter.ExcelWriter import ExcelWriter
from OrderRules.ProductWallThemeCondition import ProductWallThemeCondition
from OrderRules.ReturnOnSalesListDataFrame import ValidateReturnOnSales
from OrderRules.FranchiseListDataFrame import FranchiseListDataFrame
from OrderRules.ExchangeRateListDataFrame import ExchangeRateListDataFrame
from OrderRules.OTBListDataFrame import OTBListDataFrame
from OrderRules.RetailSalesTargetDataFrame import RetailSalesTargetDataFrame

class OrderSummaryCheck:
    def __init__(self,rules):
        self.product_level_condition_df=ProductLevelCondition(rules)
        self.quantity_level_condition_df=QuantityLevelCondition(rules)
        self.article_list_df=ValidateArticle(rules)
        self.item_master=ItemMaster(rules)
        self.article_size_list=ArticleSize(rules)
        self.bill_to_store_list_path=ValidateBilledStore(rules)
        self.validate_art_order_location_path=ValidateArticleOrderLocationType(rules)
        self.excel_writer=ExcelWriter()
        self.product_level_theme_condition=ProductWallThemeCondition(rules)
        self.return_on_sales=ValidateReturnOnSales(rules)
        self.franchise_list_path=FranchiseListDataFrame(rules)
        self.exchange_rate_list_data=ExchangeRateListDataFrame(rules)
        self.otb_list_path=OTBListDataFrame(rules)
        self.retail_sales_target=RetailSalesTargetDataFrame(rules)


    def load_all_combined_order_summary(self,rules):
        order_summary_path=rules.get_all_combined_order_summary()
        order_summary_df=pd.read_excel(order_summary_path,'All Cats Combined OS',header=0,converters={'Cat':str,'Category':str,'Factory':str,'Size':str})
        return order_summary_df

    def apply_transformation(self,df):
        # Striping function
        df=df_column_rename(df,["strip","rename"],{'Factory': 'Fac', 'StoreLocation\n(SAPShortName)': 'StoreLocation','Cat':'MPCat'},df.columns)
        df=df.applymap(lambda s:s.upper() if type(s) == str else s)
        df['Cat']=df['Final Article Number'].str.split('-',1,expand = True)[0]
        return df

    def merge_conditions(self,df):
        product_level_condition_df=self.product_level_condition_df.load_business_rules()

        product_level_condition_available_factory_df=product_level_condition_df.dropna()


        product_level_condition_df = product_level_condition_df[pd.isnull(product_level_condition_df['Fac'])]
        product_level_condition_df = product_level_condition_df.dropna(axis=1, how='all')
        product_level_condition_df = product_level_condition_df.drop_duplicates(product_level_condition_df.columns.difference(['Condition']))

        order_summary_combined_conditions_df = pd.merge(df, product_level_condition_df, how='left', on=['Cat','Category'])
        order_summary_combined_conditions_per_factory_df = pd.merge(order_summary_combined_conditions_df, product_level_condition_available_factory_df, how='left', on=['Cat','Category','Fac'],suffixes=('_dat','_con'))
        order_summary_combined_conditions_per_factory_df['Condition_con'] = order_summary_combined_conditions_per_factory_df['Condition_con'].fillna(order_summary_combined_conditions_per_factory_df['Condition_dat'])
        order_summary_combined_conditions_per_factory_df.rename(columns={'Condition_con': 'Condition'}, inplace=True)
        order_summary_combined_conditions_per_factory_df = order_summary_combined_conditions_per_factory_df.drop(['Condition_dat'],axis=1)

        return order_summary_combined_conditions_per_factory_df
    def validate_quantity(self,df):
        quantity_df=self.quantity_level_condition_df.load_business_rules()
        article_type_quantity_df=self.quantity_level_condition_df.get_article_level_qty_params(quantity_df)

        colour_type_quantity_df=self.quantity_level_condition_df.get_colour_level_qty_params(quantity_df)


        article_qty_df = df.groupby(['Final Article Number','StoreLocation','Condition'])[['Final Article Number','StoreLocation','Condition','Qty']].sum().reset_index()


        article_qty_df = pd.merge(article_qty_df,article_type_quantity_df, how='left', on=['Condition']).dropna()

        article_qty_df.insert(len(article_qty_df.columns),"Divisible", article_qty_df['Qty']%article_qty_df['Multiply Orders'])

        func=np.vectorize(self.quantity_level_condition_df.is_quantity_approved)

        article_qty_df["Status"]=func(article_qty_df['Divisible'],article_qty_df['Qty'],article_qty_df['Min Order'])

        # Colour Level Qty Check
        colour_level_qty_df = df.groupby(['Final Article Number','StoreLocation','Condition','Colour'])[['Condition','Final Article Number','StoreLocation','Qty','Colour']].sum().reset_index()
        colour_level_qty_df = pd.merge(colour_level_qty_df, colour_type_quantity_df, how='left', on=['Condition']).dropna()
        colour_level_qty_df.insert(len(colour_level_qty_df.columns),"Divisible", colour_level_qty_df['Qty']%colour_level_qty_df['Multiply Orders'])
        colour_level_qty_df["Status"] = func(colour_level_qty_df['Divisible'],colour_level_qty_df['Qty'],colour_level_qty_df['Min Order'])


        df =  df.merge(article_qty_df[['Final Article Number','StoreLocation','Condition','Status']], how='left', on=['Final Article Number','StoreLocation','Condition']).merge(colour_level_qty_df[['Final Article Number','StoreLocation','Condition','Colour','Status']], how='left', on=['Final Article Number','StoreLocation','Condition','Colour'],suffixes=('_art','_col'))

        df['Status_col'] = df['Status_col'].fillna(df['Status_art'])
        df.rename(columns={'Status_col': 'Qty_Checking'}, inplace=True)
        df = df.drop(['Status_art'],axis=1)
        return df

    def validate_article_availability(self,df):
        article_list_df= self.article_list_df.load_business_rules()
        article_list_df=self.article_list_df.get_article_list(article_list_df)
        df = pd.merge(df, article_list_df, how='left')
        df['newart'] = df['newart'].fillna(df['Final Article Number'])

        df.loc[(df['Final Article Number'].isin(article_list_df['Final Article Number']), 'Art_Checking')] = "REJECTED"
        df.loc[(df['Final Article Number'].isin(article_list_df['newart']), 'Art_Checking')] = "APPROVED"
        df['Art_Checking'].fillna("UnChecked",inplace=True)

        return df

    def validate_art_size(self,df):
        from Utils.utils import is_article_size_approved
        item_master_df=self.item_master.get_item_master()
        article_size_df=self.article_size_list.get_article_size()
        item_master_article_size_df = pd.merge(item_master_df, article_size_df, how='left', on='Size')
        item_master_article_size_df['SAPSize'] = item_master_article_size_df['SAPSize'].fillna(item_master_article_size_df['Size'])

        item_master_article_size_df = item_master_article_size_df.drop(['Size'],axis=1)
        item_master_article_size_df.rename(columns={'SAPSize': 'Size'}, inplace=True)
        item_master_article_size_df.insert(len(item_master_article_size_df.columns),"SBatch",  item_master_article_size_df['ArticleNo']+item_master_article_size_df['Size'])
        is_article_size_approved=is_article_size_approved(df,item_master_article_size_df['SBatch'].drop_duplicates().tolist(),item_master_article_size_df['ArticleNo'].drop_duplicates().tolist())
        df['Size_Checking']=is_article_size_approved
        return df,item_master_df

    def validate_art_colour_code(self,df,item_master_df):
        from Utils.utils import is_article_colour_approved
        item_master_df.insert(len(item_master_df.columns),"Batch", item_master_df['ArticleNo']+item_master_df['ColorName'])
        is_article_colour_approved=is_article_colour_approved(df,item_master_df['Batch'].drop_duplicates().tolist(),item_master_df['ArticleNo'].drop_duplicates().tolist())

        df['Col_Checking']=is_article_colour_approved
        return df,item_master_df


    def validate_billed_store(self,df):
        bill_to_shop_list=self.bill_to_store_list_path.get_billed_shop_path_list()

        df.loc[(df['Customer'].isin(bill_to_shop_list['shipto_name ']), 'Customer_Checking')] = "APPROVED"
        df['Customer_Checking'].fillna("REJECTED",inplace=True)

        df.loc[(df['StoreLocation'].isin(bill_to_shop_list['Ship-To/Store(Short Code)']), 'StoreCode_Checking')] = "APPROVED"
        df['StoreCode_Checking'].fillna("REJECTED",inplace=True)

        return df

    def validate_art_order_location(self,df):
        from Utils.utils import is_article_location_approved
        art_order_locations_df=self.validate_art_order_location_path.load_business_rules()
        art_order_locations_df=self.validate_art_order_location_path.get_order_location_type(art_order_locations_df,"WH")

        df_country = df.groupby(['Final Article Number','StoreLocation','Condition','Country'])[['Qty','Condition','Final Article Number','StoreLocation','Country']].sum().reset_index()
        df_country_count = df_country.groupby(['Final Article Number','Condition','Country']).agg({'Qty':'sum','StoreLocation':'count'}).reset_index()

        is_article_location_approved=is_article_location_approved(df_country_count,art_order_locations_df['Condition'].tolist())

        df_country_count["Status"]=is_article_location_approved

        df = pd.merge(df, df_country_count[['Final Article Number','Status','Country']].drop_duplicates(), how='left', on=['Final Article Number','Country'])

        df.rename(columns={'Status': 'Location_Checking'}, inplace=True)
        return df

    def remove_duplicates(self,df):
        df['Duplicate_Checking'] = df.duplicated(keep=False)
        df['Duplicate_Checking'] = df['Duplicate_Checking'].replace([False,True],["APPROVED","REJECTED"])

        df_rejected = df[(df == 'REJECTED').any(axis=1)]
        df_rejected_summary = df_rejected.groupby(['Country',
                                      'Cat',
                                       'Qty_Checking',
                                       'Art_Checking',
                                       'Size_Checking',
                                       'Col_Checking',
                                       'Customer_Checking',
                                       'StoreCode_Checking',
                                       'Location_Checking',
                                       'Duplicate_Checking'])[['Country',
                                                               'Cat',
                                                               'Qty_Checking',
                                                               'Art_Checking',
                                                               'Size_Checking',
                                                               'Col_Checking',
                                                               'Customer_Checking',
                                                               'StoreCode_Checking',
                                                               'Location_Checking',
                                                               'Duplicate_Checking']].last()

        df_rejected_summary = df_rejected_summary.iloc[:,:0].reset_index()
        return df_rejected,df_rejected_summary,df

    def export_final_summer_combined_excel(self,writer,df,sheet_name):

        is_excel_writen=self.excel_writer.write_excel(writer,df,sheet_name)
        return is_excel_writen

    def validate_wall_theme_condition(self,df,item_master_df):
        from Utils.utils import is_article_category_approved
        product_level_theme_condition_df=self.product_level_theme_condition.get_product_level_theme_condition()
        item_master_df = pd.merge(item_master_df, product_level_theme_condition_df, how='left',left_on="Theme",right_on="sku_theme text")

        df = pd.merge(df, item_master_df[['ArticleNo','ColorName','Theme','Group']], how='left', left_on=['newart','Colour'], right_on=['ArticleNo','ColorName'])
        df = df.drop([
            'Qty_Checking',
            'Art_Checking',
            'Size_Checking',
            'Col_Checking',
            'Customer_Checking',
            'StoreCode_Checking',
            'Location_Checking',
            'Duplicate_Checking'],axis=1)
        article_theme_qty = df[pd.isnull(df['Group'])].groupby(['Country','Theme','Colour','Cat'])['Qty','newart'].agg({'Qty':'sum','newart':'count'}).reset_index()

        article_theme_qty_ck = article_theme_qty.loc[article_theme_qty['Cat'].isin(['CK1','CK2'])].set_index(['Country','Theme','Colour','Cat'])
        article_theme_qty_ck.rename(columns={'newart': 'Art'}, inplace=True)
        article_theme_qty_ck = article_theme_qty_ck.unstack('Cat').reset_index()
        article_theme_qty_ck=combine_columns(article_theme_qty_ck,split_by='')

        func = np.vectorize(is_article_category_approved)
        article_theme_qty_ck['IsThemeApproved']=func(article_theme_qty_ck['QtyCK1'],article_theme_qty_ck['QtyCK2'])

        return article_theme_qty_ck,df,item_master_df

    def validate_article_theme_colour_by_country(self,article_theme_qty_ck):
        from Utils.utils import IsThemeOverallApproved
        article_matching_theme_count=article_theme_qty_ck.groupby(['Country','Theme','IsThemeApproved']).count()
        article_matching_theme_count = article_matching_theme_count.iloc[:,:1].reset_index()
        article_matching_theme_count = article_matching_theme_count.set_index(['Country','Theme','IsThemeApproved']).unstack('IsThemeApproved').reset_index()
        to_column= list(article_matching_theme_count.columns.get_level_values(level=0))
        from_column= list(article_matching_theme_count.columns.get_level_values(level=1))
        new_col_name=pd.Series(from_column).replace('', pd.Series(to_column), regex=True)
        article_matching_theme_count.set_axis(new_col_name, axis='columns', inplace=True)
        article_matching_theme_count = article_matching_theme_count.fillna(0)
        func = np.vectorize(IsThemeOverallApproved)

        article_matching_theme_count['Overall_Checking']=func(article_matching_theme_count['APPROVED'],article_matching_theme_count['REJECTED'],article_matching_theme_count['UnChecked'])

        article_theme_qty_ck = pd.merge(article_theme_qty_ck, article_matching_theme_count.loc[:,['Country','Theme','Overall_Checking']], how='left',on=['Country','Theme'])
        article_theme_qty_ck = article_theme_qty_ck.set_index(['Country','Theme']).sort_index()

        return article_theme_qty_ck

    def validate_item_group_offer(self,df,item_master_df):
        from Utils.utils import IsItemGroupCategoryOfferApproved
        df_group_by_country= df.groupby(['Country','Group','Colour','Cat','newart'])['Qty'].sum().reset_index()
        df_group_by_country_count = df_group_by_country.groupby(['Country','Group','Cat'])['newart'].count().reset_index()
        df_group = item_master_df.loc[:,['ArticleNo','Group']].drop_duplicates().dropna()



        df_group['Cat'] =df_group['ArticleNo'].str.split('-',1,expand = True)[0]

        df_group_count = df_group.groupby(['Group','Cat']).count().reset_index()
        df_group_count.rename(columns={'ArticleNo': 'Offer'}, inplace=True)


        df_group_by_country_count = pd.merge(df_group_by_country_count,df_group_count,how='left',on=['Group','Cat'])

        func=np.vectorize(IsItemGroupCategoryOfferApproved)
        df_group_by_country_count["GroupCat_Checking"] = func(df_group_by_country_count['newart'],df_group_by_country_count['Offer'])

        return df_group_by_country_count,df_group_by_country


    def validate_item_group_total(self,item_master_df,df_group_by_country_count,df_group_by_country):
        from Utils.utils import IsItemGroupCategoryOfferApproved
        df_item_group_offer= item_master_df.loc[:,['ArticleNo','Group','ColorName']].drop_duplicates().dropna()
        df_item_group_offer['Cat']=df_item_group_offer['ArticleNo'].str.split('-',1,expand = True)[0]

        df_item_group_offer_count = df_item_group_offer.groupby(['Group','Cat','ColorName']).count().reset_index()
        df_item_group_offer_count.rename(columns={'ArticleNo': 'Offer', 'ColorName': 'Colour'}, inplace=True)

        df_item_group_countrycount = df_group_by_country.groupby(['Country','Group','Colour','Cat']).agg({'newart':'count'}).reset_index()
        df_item_group_countrycount = pd.merge(df_item_group_countrycount,df_item_group_offer_count,how='left',on=['Group','Cat','Colour'])
        df_item_group_countrycount.rename(columns={'newart': 'Art'}, inplace=True)


        df_item_group_countrycount = df_item_group_countrycount.set_index(['Country','Group','Colour','Cat']).unstack('Cat').reset_index()
        df_item_group_countrycount = df_item_group_countrycount.fillna(0)


        df_item_group_countrycount[('Art','Total')]= df_item_group_countrycount[('Art','CK2')] + df_item_group_countrycount[('Art','CK6')]
        df_item_group_countrycount[('Offer','Total')]= df_item_group_countrycount[('Offer','CK2')] + df_item_group_countrycount[('Offer','CK6')] # Get totals
        df_item_group_countrycount = df_item_group_countrycount.set_index(['Country','Group','Colour']).sort_index(axis=1).reset_index() # Order columns nicely


        df_group_by_country_count=df_group_by_country_count.merge(df_item_group_countrycount,how="inner",on=["Country","Group"])
        df_item_group_countrycount[('CK2_Checking','')]=df_group_by_country_count.query("Cat=='CK2'")["GroupCat_Checking"].iat[0]
        df_item_group_countrycount[('CK6_Checking','')]=df_group_by_country_count.query("Cat=='CK6'")["GroupCat_Checking"].iat[0]

        func=np.vectorize(IsItemGroupCategoryOfferApproved)
        df_item_group_countrycount['GroupCol_Checking']=func(df_item_group_countrycount[('Art','Total')].astype(int),df_item_group_countrycount[('Offer','Total')].astype(int))

        return df_item_group_countrycount


    def validate_return_on_sales(self,df):
        from Utils.utils import get_cumulative_grading,get_art_depth_grading,get_grading_percentage,get_country_level_order_grading
        df_return_on_sales=self.return_on_sales.get_return_on_sales()
        df_return_on_sales=self.return_on_sales.apply_transformation(df_return_on_sales)

        df_return_on_sales['Country'],df_return_on_sales['Value_Type']=df_return_on_sales['variable'].str.split('_',1).str


        df_return_on_sales = df_return_on_sales.drop(['variable'],axis=1)
        df_return_on_sales['Cat']=df_return_on_sales['ArticleNo'].str.split('-',1,expand = True)[0]
        df_return_on_sales = df_return_on_sales.applymap(lambda s:s.upper() if type(s) == str else s)

        df_return_on_sales = df_return_on_sales[df_return_on_sales['Launch'].isin(['Total'])]

        df_return_on_sales = df_return_on_sales[~df_return_on_sales['value'].isin(['-'])]

        df_return_on_sales = df_return_on_sales[df_return_on_sales['value'] > 0]
        df_return_on_sales['value'] = pd.to_numeric(df_return_on_sales['value'])

        filtertype = '8 Weeks ROS' # Adjust what statistics you want to see here

        df_weekly_return_on_sales = df_return_on_sales[df_return_on_sales['Value_Type'].isin([filtertype])] #Filter out filtertype

        df_weekly_return_on_sales = df_weekly_return_on_sales.drop(['index','Launch','Colour'],axis=1)
        df_weekly_return_on_sales = df_weekly_return_on_sales.sort_values(by = ['Country','Cat','value'], ascending=False)



        n = len(df_weekly_return_on_sales)
        cumsumresult=[]

        for row in range(0,n):
            if row == 0:
                cumsumresult.append(df_weekly_return_on_sales['value'].iloc[row])
            else:
                prow = row - 1
                if df_weekly_return_on_sales['Country'].iloc[row] == df_weekly_return_on_sales['Country'].iloc[prow] and df_weekly_return_on_sales['Cat'].iloc[row] == df_weekly_return_on_sales['Cat'].iloc[prow]:
                    val = df_weekly_return_on_sales['value'].iloc[row] + cumsumresult[prow]
                    cumsumresult.append(val)
                else:
                    cumsumresult.append(df_weekly_return_on_sales['value'].iloc[row])


        df_weekly_return_on_sales['CumulativeSum'] = cumsumresult

        df_weekly_return_on_sales_total = df_weekly_return_on_sales.groupby(['Country','Cat'])['value'].sum().reset_index()
        df_weekly_return_on_sales_total.Country= df_weekly_return_on_sales_total.Country.astype(str)
        df_weekly_return_on_sales_total.Cat= df_weekly_return_on_sales_total.Cat.astype(str)

        df_weekly_return_on_sales = pd.merge(df_weekly_return_on_sales, df_weekly_return_on_sales_total,how='left',on=['Country','Cat'],suffixes=['','_total'])


        df_weekly_return_on_sales['Cumulative%'] = df_weekly_return_on_sales['CumulativeSum']/ df_weekly_return_on_sales['value_total']

        func=np.vectorize(get_cumulative_grading)



        df_weekly_return_on_sales['Actual_Grading'] = func(df_weekly_return_on_sales['Cumulative%'])



        df_return_on_sales_summary = df_weekly_return_on_sales.groupby(['Country','Cat','Actual_Grading'])['value'].min().unstack().reset_index()

        df_article_count_by_country = df.groupby(['Country','Cat','newart'])['Country','Cat','newart','Qty'].sum().reset_index()
        df_article_count_by_country = pd.merge(df_article_count_by_country, df_return_on_sales_summary , how='left', on=['Country','Cat'])

        func=np.vectorize(get_art_depth_grading)

        df_article_count_by_country["Grading"] = func(df_article_count_by_country['Qty'],df_article_count_by_country['A'],df_article_count_by_country['B'])



        df_return_on_sales_art_count = df_weekly_return_on_sales.groupby(['Country','Cat','Actual_Grading'])['value'].count().unstack('Actual_Grading').reset_index()
        df_return_on_sales_art_count = df_return_on_sales_art_count.fillna(0)

        func=np.vectorize(get_grading_percentage)

        total = df_return_on_sales_art_count['A']+df_return_on_sales_art_count['B']+df_return_on_sales_art_count['C']
        df_return_on_sales_art_count['A%'] = func(df_return_on_sales_art_count['A'],total)
        df_return_on_sales_art_count['B%'] = func(df_return_on_sales_art_count['B'],total)
        df_return_on_sales_art_count['C%'] = func(df_return_on_sales_art_count['C'],total)


        df_return_on_sales_total_art= df_weekly_return_on_sales.groupby(['Country','Cat','Actual_Grading'])['value'].sum().unstack('Actual_Grading').reset_index()

        df_return_on_sales_total_art = df_return_on_sales_total_art.fillna(0)

        total = df_return_on_sales_total_art['A']+df_return_on_sales_total_art['B']+df_return_on_sales_total_art['C']
        df_return_on_sales_total_art['A%'] = func(df_return_on_sales_total_art['A'],total)
        df_return_on_sales_total_art['B%'] = func(df_return_on_sales_total_art['B'],total)
        df_return_on_sales_total_art['C%'] = func(df_return_on_sales_total_art['C'],total)



        df_return_on_sales_count_art_by_country = df_article_count_by_country.groupby(['Country','Cat','Grading'])['Qty'].count().unstack('Grading').reset_index()
        df_return_on_sales_count_art_by_country = df_return_on_sales_count_art_by_country.fillna(0)
        #
        total =  df_return_on_sales_count_art_by_country['A']+df_return_on_sales_count_art_by_country['B']+df_return_on_sales_count_art_by_country['C']
        df_return_on_sales_count_art_by_country['A%'] = func(df_return_on_sales_count_art_by_country['A'],total)
        df_return_on_sales_count_art_by_country['B%'] = func(df_return_on_sales_count_art_by_country['B'],total)
        df_return_on_sales_count_art_by_country['C%'] = func(df_return_on_sales_count_art_by_country['C'],total)

        #
        #
        df_return_on_sales_total_art_by_country = df_article_count_by_country.groupby(['Country','Cat','Grading'])['Qty'].sum().unstack('Grading').reset_index()
        df_return_on_sales_total_art_by_country = df_return_on_sales_total_art_by_country.fillna(0)
        #
        total =  df_return_on_sales_total_art_by_country['A']+df_return_on_sales_total_art_by_country['B']+df_return_on_sales_total_art_by_country['C']
        df_return_on_sales_total_art_by_country['A%'] = func(df_return_on_sales_total_art_by_country['A'],total)
        df_return_on_sales_total_art_by_country['B%'] = func(df_return_on_sales_total_art_by_country['B'],total)
        df_return_on_sales_total_art_by_country['C%'] = func(df_return_on_sales_total_art_by_country['C'],total)


        df_return_on_sales_total_distribution_count = pd.merge(df_return_on_sales_count_art_by_country,df_return_on_sales_art_count.loc[:,['Country','Cat','A%', 'B%', 'C%']],
                                           how='left',on=['Country','Cat'], suffixes=['_Order','_Sales'])
        df_return_on_sales_total_distribution_sum = pd.merge(df_return_on_sales_total_art_by_country,df_return_on_sales_total_art.loc[:,['Country','Cat','A%', 'B%', 'C%']],
                                         how='left',on=['Country','Cat'], suffixes=['_Order','_Sales'])
        #
        func=np.vectorize(get_country_level_order_grading)


        df_return_on_sales_total_distribution_count['A%_Checking'] = func(df_return_on_sales_total_distribution_count['A%_Order'],df_return_on_sales_total_distribution_count['A%_Sales'])
        df_return_on_sales_total_distribution_count['B%_Checking'] = func(df_return_on_sales_total_distribution_count['B%_Order'],df_return_on_sales_total_distribution_count['B%_Sales'])
        df_return_on_sales_total_distribution_count['C%_Checking'] = func(df_return_on_sales_total_distribution_count['C%_Order'],df_return_on_sales_total_distribution_count['C%_Sales'])

        df_return_on_sales_total_distribution_sum['A%_Checking'] = func(df_return_on_sales_total_distribution_sum['A%_Order'],df_return_on_sales_total_distribution_sum['A%_Sales'])
        df_return_on_sales_total_distribution_sum['B%_Checking'] = func(df_return_on_sales_total_distribution_sum['B%_Order'],df_return_on_sales_total_distribution_sum['B%_Sales'])
        df_return_on_sales_total_distribution_sum['C%_Checking'] = func(df_return_on_sales_total_distribution_sum['C%_Order'],df_return_on_sales_total_distribution_sum['C%_Sales'])



        filtertype='Initial Order'
        df_article_initial_order_return_on_sales = df_return_on_sales[df_return_on_sales['Value_Type'].isin([filtertype])]


        df_article_initial_order_return_on_sales = df_article_initial_order_return_on_sales.drop(['index','Launch','Colour'],axis=1)
        df_article_initial_order_return_on_sales = pd.merge(df_article_initial_order_return_on_sales, df_weekly_return_on_sales.loc[:,['ArticleNo','Country','Actual_Grading']],how='left',on=['ArticleNo','Country'])
        df_article_initial_order_return_on_sales_total = df_article_initial_order_return_on_sales.groupby(['Country','Cat','Actual_Grading'])['value'].sum().unstack('Actual_Grading').reset_index()
        df_article_initial_order_return_on_sales_total = df_article_initial_order_return_on_sales_total.fillna(0)

        print df_article_initial_order_return_on_sales_total.columns
        total = df_article_initial_order_return_on_sales_total['A']+df_article_initial_order_return_on_sales_total['B']+df_article_initial_order_return_on_sales_total['C']
        df_article_initial_order_return_on_sales_total['A%_HistIO'] = func(df_article_initial_order_return_on_sales_total['A'],total)
        df_article_initial_order_return_on_sales_total['B%_HistIO'] = func(df_article_initial_order_return_on_sales_total['B'],total)
        df_article_initial_order_return_on_sales_total['C%_HistIO'] = func(df_article_initial_order_return_on_sales_total['C'],total)

        #
        df_return_on_sales_total_distribution_sum = pd.merge(df_return_on_sales_total_distribution_sum,
                                                             df_article_initial_order_return_on_sales_total.loc[:,['Country','Cat','A_GradeInitialOrderPercentage', 'B_GradeInitialOrderPercentage', 'C_GradeInitialOrderPercentage']],
                                         how='left',on=['Country','Cat'])


        return df_article_count_by_country,df_return_on_sales_total_distribution_count,df_return_on_sales_total_distribution_sum,df_return_on_sales_art_count,df_return_on_sales_total_art




    def get_open_to_buy_customers(self,df_rejected_original):
        retail_sales_target_df=self.retail_sales_target.get_retail_sales_target_list()

        bill_to_shop_list=self.bill_to_store_list_path.get_billed_shop_path_list()

        franchise_list_df=self.franchise_list_path.load_franchise_list_path()
        shipbilltounique = bill_to_shop_list.loc[:,['customer_code','shipto_name ']]
        shipbilltounique = shipbilltounique.drop_duplicates() # Removes duplicates
        franchise_list_df = pd.merge(franchise_list_df, shipbilltounique,how='left',left_on='Customer',right_on='customer_code')
        exchange_rate_df=self.exchange_rate_list_data.get_exchange_rate_list()
        franchise_list_df = pd.merge(franchise_list_df, exchange_rate_df,how='left',on='Currency')
        franchise_list_df.rename(columns={'TTL Value (QTYxUnit Price)': 'UnitPrice'}, inplace=True) # Cat labelled by MP

        franchise_list_df['SGD_UnitPrice'] =  franchise_list_df['UnitPrice'] / franchise_list_df['X_Rate']

        print("Misspell Check",df_rejected_original.loc[df_rejected_original['Customer_Checking'].isin(['REJECTED']),['Customer','Customer_Checking']].drop_duplicates())

        misspelled = {'RETAIL INTERNATIONAL GROUP': 'RETAIL INTERNATIONAL LTD'}
        df_rejected_original = df_rejected_original.replace({'Customer': misspelled})
        print("Misspell Check",df_rejected_original.loc[df_rejected_original['Customer_Checking'].isin(['REJECTED']),['Customer','Customer_Checking']].drop_duplicates())
        misspelled_df = pd.DataFrame(data=misspelled, index=[0])




        df_rejected_original = pd.merge(df_rejected_original, franchise_list_df.loc[:,['Currency','UnitPrice','SGD_UnitPrice','Material','shipto_name ']], how='left', left_on=['newart','Customer'], right_on=['Material','shipto_name '])
        df_rejected_original = df_rejected_original.drop([
            'Qty_Checking',
            'Art_Checking',
            'Size_Checking',
            'Col_Checking',
            'StoreCode_Checking',
            'Location_Checking',
            'Duplicate_Checking'],axis=1)


        df_rejected_original['SGD'] =  df_rejected_original['SGD_UnitPrice'] * df_rejected_original['Qty']
        otb_list=self.otb_list_path.load_otb_list_path()

        df_rejected_original = pd.merge(df_rejected_original,otb_list,  how='left', on='Cat')




        otb_customer_list = df_rejected_original.groupby(['Country','Customer','OTB_Cat'])['Country','Customer','OTB_Cat','Qty','SGD'].sum().unstack('OTB_Cat')

        otb_customer_list = otb_customer_list.swaplevel(axis=1)
        otb_customer_list = otb_customer_list.fillna(0)
        otb_customer_list[('Total','Qty')] = otb_customer_list[('CK Shoes','Qty')] + otb_customer_list[('Kid Bags','Qty')] + otb_customer_list[('Bags','Qty')] + otb_customer_list[('Shades','Qty')] + otb_customer_list[('Belts','Qty')] + otb_customer_list[('CJ','Qty')] + otb_customer_list[('SLG','Qty')] + otb_customer_list[('Accs','Qty')] + otb_customer_list[('SL Shoes','Qty')] + otb_customer_list[('Kid Shoes','Qty')]
        #+ otb_customer[('Footcare','Qty')]
        otb_customer_list[('Total','SGD')] = otb_customer_list[('CK Shoes','SGD')] + otb_customer_list[('Kid Bags','SGD')] + otb_customer_list[('Bags','SGD')] + otb_customer_list[('Shades','SGD')] + otb_customer_list[('Belts','SGD')] + otb_customer_list[('CJ','SGD')] + otb_customer_list[('SLG','SGD')] + otb_customer_list[('Accs','SGD')] + otb_customer_list[('SL Shoes','SGD')] + otb_customer_list[('Kid Shoes','SGD')]
        #+ otb_customer[('Footcare','SGD')]
        otb_customer_list = otb_customer_list.sort_values(by = [('Total','SGD')], ascending=False)
        column_order = [('Total', 'Qty'), ('Total', 'SGD'),
                        ('CK Shoes', 'Qty'),('CK Shoes', 'SGD'),
                        ('SL Shoes', 'Qty'),('SL Shoes', 'SGD'),
                        ('Kid Shoes', 'Qty'),('Kid Shoes', 'SGD'),
                        ('Bags', 'Qty'),('Bags', 'SGD'),
                        ('SLG', 'Qty'),('SLG', 'SGD'),
                        ('Kid Bags', 'Qty'),('Kid Bags', 'SGD'),
                        ('Belts', 'Qty'), ('Belts', 'SGD'),
                        ('Shades', 'Qty'),('Shades', 'SGD'),
                        ('CJ', 'Qty'), ('CJ', 'SGD'),
                        ('Accs', 'Qty'), ('Accs', 'SGD')]
        otb_customer_list = otb_customer_list[column_order]

        otb_mpnames = df_rejected_original.loc[:,['Customer','Added By']].drop_duplicates().reset_index(drop=True)
        otb_mpnames = otb_mpnames.sort_values(by = ['Added By'], ascending=True)
        otb_mpnames['CKI Inv'] = otb_mpnames['Added By']+' & '
        otb_mpnames = otb_mpnames.groupby('Customer')['CKI Inv'].sum().reset_index()
        otb_mpnames['CKI Inv'] = otb_mpnames['CKI Inv'].astype(str).str[:-3]

        otb_customer_list = otb_customer_list.reset_index()
        otb_customer_merge = pd.merge(otb_customer_list,otb_mpnames,how='left',on='Customer')['CKI Inv']
        otb_customer_list.insert(0,"CKI Inv", otb_customer_merge)

        otb_self_run_countries=self.otb_list_path.load_otb_self_run_countries()
        otb_self_run_countries = otb_self_run_countries.applymap(lambda s:s.upper() if type(s) == str else s)
        otb_self_run_countries_list = otb_self_run_countries['Country'].values.tolist()
        cki_otb_customer_list = otb_customer_list[~otb_customer_list['Country'].isin(otb_self_run_countries_list)]


        otb_customer_list.loc['Grand Total'] = otb_customer_list.sum() # sum rows
        otb_customer_list.loc['Grand Total',[('CKI Inv', '')]] = 'GRAND TOTAL'
        otb_customer_list.loc['Grand Total',[('Country', '')]] = 'GRAND TOTAL'
        otb_customer_list.loc['Grand Total',[('Customer', '')]] = 'GRAND TOTAL'

        cki_otb_customer_list = cki_otb_customer_list.reset_index(drop=True)
        cki_otb_customer_list.loc['Grand Total'] = cki_otb_customer_list.sum() # sum rows
        cki_otb_customer_list.loc['Grand Total',[('CKI Inv', '')]] = 'GRAND TOTAL'
        cki_otb_customer_list.loc['Grand Total',[('Country', '')]] = 'GRAND TOTAL'
        cki_otb_customer_list.loc['Grand Total',[('Customer', '')]] = 'GRAND TOTAL'



        otb_customer_list = otb_customer_list.set_index(['CKI Inv', 'Country', 'Customer'])
        otbcattemp = []
        TotQty = otb_customer_list.iloc[-1, 0]
        TotSGD = otb_customer_list.iloc[-1, 1]
        NCol = len(otb_customer_list.iloc[-1])
        for i in range(0, NCol):
            if i % 2 == 0: #even number
                otbcattemp.append(otb_customer_list.iloc[-1, i]/ TotQty)
            else:
                otbcattemp.append(otb_customer_list.iloc[-1, i] / TotSGD)
        #otb_customer = otb_customer.reset_index()
        otb_customer_list.loc[('%', '%','%')] = otbcattemp


        cki_otb_customer_list = cki_otb_customer_list.set_index(['CKI Inv', 'Country', 'Customer'])
        otbcattemp = []
        TotQty = cki_otb_customer_list.iloc[-1, 0]
        TotSGD = cki_otb_customer_list.iloc[-1, 1]
        NCol = len(cki_otb_customer_list.iloc[-1])
        for i in range(0, NCol):
            if i % 2 == 0: #even number
                otbcattemp.append(cki_otb_customer_list.iloc[-1, i]/ TotQty)
            else:
                otbcattemp.append(cki_otb_customer_list.iloc[-1, i] / TotSGD)
        #otb_customer = otb_customer.reset_index()
        cki_otb_customer_list.loc[('%', '%','%')] = otbcattemp

        retail_sales_target_df_tot = retail_sales_target_df.loc[:,['Country','Total','Total.1']].reset_index(drop=True).drop_duplicates()
        retail_sales_target_df_tot.columns = ['Country','Qty','SGD']
        retail_sales_target_df_tot['Target'] = 'Target'
        retail_sales_target_df_tot = retail_sales_target_df_tot.reset_index(drop=True).groupby(['Country','Target']).sum().unstack('Target')
        retail_sales_target_df_tot = retail_sales_target_df_tot.swaplevel(axis=1)


        otb_customer_list['']=''
        otb_customer_list = pd.merge(otb_customer_list.reset_index(),retail_sales_target_df_tot.reset_index(),how='left',on=[('Country', '')])
        otb_customer_list.loc[otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','Qty')]] = \
            otb_customer_list.loc[~otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','Qty')]][('Target','Qty')].sum()

        otb_customer_list.loc[otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','SGD')]] = \
            otb_customer_list.loc[~otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','SGD')]][('Target','SGD')].sum()

        otb_customer_list[('Target','Qty +/-')] = otb_customer_list[('Total','Qty')] - otb_customer_list[('Target','Qty')]
        otb_customer_list[('Target','Var Qty')] = otb_customer_list[('Target','Qty +/-')]/ otb_customer_list[('Target','Qty')]
        otb_customer_list[('Target','SGD +/-')] = otb_customer_list[('Total','SGD')] - otb_customer_list[('Target','SGD')]
        otb_customer_list[('Target','Var SGD')] = otb_customer_list[('Target','SGD +/-')]/ otb_customer_list[('Target','SGD')]



        cki_otb_customer_list['']=''
        cki_otb_customer_list = pd.merge(cki_otb_customer_list.reset_index(),retail_sales_target_df_tot.reset_index(),how='left',on=[('Country', '')])
        cki_otb_customer_list.loc[cki_otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','Qty')]] = \
            cki_otb_customer_list.loc[~cki_otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','Qty')]][('Target','Qty')].sum() # SUM ALL EXCEPT GRAND TOTAL CELL
        cki_otb_customer_list.loc[cki_otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','SGD')]] = \
            cki_otb_customer_list.loc[~cki_otb_customer_list['Country'].isin(['GRAND TOTAL']),[('Target','SGD')]][('Target','SGD')].sum() # SUM ALL EXCEPT GRAND TOTAL CELL
        cki_otb_customer_list[('Target','Qty +/-')] = cki_otb_customer_list[('Total','Qty')] - cki_otb_customer_list[('Target','Qty')]
        cki_otb_customer_list[('Target','Var Qty')] = cki_otb_customer_list[('Target','Qty +/-')]/ cki_otb_customer_list[('Target','Qty')]
        cki_otb_customer_list[('Target','SGD +/-')] = cki_otb_customer_list[('Total','SGD')] - cki_otb_customer_list[('Target','SGD')]
        cki_otb_customer_list[('Target','Var SGD')] = cki_otb_customer_list[('Target','SGD +/-')]/ cki_otb_customer_list[('Target','SGD')]

        return otb_customer_list,cki_otb_customer_list,franchise_list_df

    def run_order_summary_check(self,rules):

        df=self.load_all_combined_order_summary(rules)
        df=self.apply_transformation(df)



        df=self.merge_conditions(df)



        df=self.validate_quantity(df)

        print "Validated Article Quantity"
        df=self.validate_article_availability(df)

        print "Validated Article"
        df,item_master_df=self.validate_art_size(df)

        print "Validated Article Size"
        df,item_master_df=self.validate_art_colour_code(df,item_master_df)
        print "Validated Article Colour"

        df=self.validate_billed_store(df)
        print "Validated Customer and Store Code"

        df=self.validate_art_order_location(df)
        print "Validated Store Location"

        df_rejected,df_rejected_summary,df_rejected_original=self.remove_duplicates(df)


        print "Removed Duplicates"

        article_theme_qty_ck,df,item_master_df=self.validate_wall_theme_condition(df,item_master_df)

        print "Validated Wall Theme Condition"
        article_theme_qty_ck=self.validate_article_theme_colour_by_country(article_theme_qty_ck)
        print "Validated Wall Theme Colour Condition"
        df_group_by_country_count,df_group_by_country=self.validate_item_group_offer(df,item_master_df)
        print "Validated Item Group Offer"
        df_item_group_countrycount=self.validate_item_group_total(item_master_df,df_group_by_country_count,df_group_by_country)
        print "Validated Item Group Total"

        df_article_count_by_country,df_return_on_sales_total_distribution_count,df_return_on_sales_total_distribution_sum,df_return_on_sales_art_count,df_return_on_sales_total_art= \
            self.validate_return_on_sales(df)
        print "Validated ROS Data"

        otb_customer_list,cki_otb_customer_list,franchise_list_df=self.get_open_to_buy_customers(df_rejected_original)


        return df_rejected,df_rejected_summary,article_theme_qty_ck,df_item_group_countrycount,item_master_df,df_article_count_by_country,df_return_on_sales_total_distribution_count,\
                df_return_on_sales_total_distribution_sum,df_return_on_sales_art_count,df_return_on_sales_total_art,otb_customer_list,cki_otb_customer_list,franchise_list_df
