import pandas as pd
from  Rules.Rules import Rules
from  Utils.utils import combine_columns

class ExchangeRateListDataFrame():
    def __init__(self,rules):
        self.rules=rules

    def get_exchange_rate_list(self):
        exchange_rate_path=self.rules.get_business_rule_path()
        df = pd.read_excel(exchange_rate_path,'XR').iloc[:,:2]
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        df.columns = ['Currency','X_Rate']
        df = df.append(pd.DataFrame([["SGD",1]], columns=list(df)))
        return df













