import pandas as pd
from  Rules.Rules import Rules


class ArticleSize():
    def __init__(self,rules):
        self.rules=rules

    def get_article_size(self):
        business_rule_path=self.rules.get_business_rule_path()
        df = pd.read_excel(business_rule_path,'Size')
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df






