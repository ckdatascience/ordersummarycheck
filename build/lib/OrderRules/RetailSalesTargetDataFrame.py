import pandas as pd
from  Rules.Rules import Rules


class RetailSalesTargetDataFrame():

    def __init__(self,rules):
        self.rules=rules

    def get_retail_sales_target_list(self):
        retail_sales_target=self.rules.get_retail_sales_target_path()
        df = pd.read_excel(retail_sales_target,'OTB Summer + SS JIT 19',skiprows=1)
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df

