import pandas as pd
from  Rules.Rules import Rules


class ItemMaster():
    def __init__(self,rules):
        self.rules=rules

    def get_item_master(self):
        item_master=self.rules.get_item_master_path()
        df = pd.read_csv(item_master,header=0,converters={'SupplierCode':str,'Size':str})
        df = df.applymap(lambda s:s.upper() if type(s) == str else s)
        return df
