import pandas as pd
from  Rules.Rules import Rules
from  Utils.utils import combine_columns

class ValidateReturnOnSales():

    def __init__(self,rules):
        self.rules=rules

    def get_return_on_sales(self):
        return_on_sales_path=self.rules.get_return_on_sales_path()
        df = pd.read_excel(return_on_sales_path,header=[0,1])
        df = df.reset_index()
        return df

    def apply_transformation(self,df):
        df = df.reset_index()
        df = df.drop(['Unnamed: 0_level_0'],axis=1)
        df.dropna(axis='columns')
        df=combine_columns(df)
        df.rename(columns={'index_': 'ArticleNo', 'Unnamed: 1_level_0_Launch': 'Launch', 'Country_Color': 'Colour'}, inplace=True)
        df = df.melt(['ArticleNo','Launch','Colour']).reset_index()
        return df













