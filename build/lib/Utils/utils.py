import pandas as pd
def df_column_rename(df,functions,rename_col_dict=None,transform_columns=[]):
    for function in functions:
        if function=="strip":
            df.rename(columns={column:column.strip() for column in transform_columns},inplace=True)
        if function=="rename":
            df.rename(columns=rename_col_dict,inplace=True)

    return df


def combine_columns(data,split_by="_"):
    colname0= list(data.columns.get_level_values(level=0))
    colname1= list(data.columns.get_level_values(level=1))
    colname = pd.Series(colname0) + split_by + pd.Series(colname1)
    data.set_axis(colname, axis='columns', inplace=True)
    return data


def is_article_category_approved(CK1,CK2):
    if CK1 > 1 and CK2 > 1 :
        return "APPROVED"
    elif CK1 > 1 :
        return "REJECTED"
    else:
        return "UnChecked"

def IsThemeOverallApproved(approved,rejected,missing):

    if approved == 1 & (rejected > 0 or missing > 0) :
        return "REJECTED" # Only 1 matching colour or less when other col exists
    elif approved >=1: # Two or more matching colours of shoe to bag in a theme
        return "APPROVED"
    else:
        return "UnChecked"


def IsItemGroupCategoryOfferApproved(art_count,offer):
    if art_count > 2 or (offer < 4 and  art_count > 1):
        return "APPROVED"
    else:
        return "REJECTED"


def get_cumulative_grading(cump,ab = 0.5,bc=0.8):
    if cump < ab :
        return "A"
    elif cump < bc :
        return "B"
    else:
        return "C"


def get_art_depth_grading(qty, A, B, WT=13):
    import numpy as np
    if qty > A * WT :
        return "A"
    elif qty > B * WT :
        return "B"
    elif np.isnan(A) or np.isnan(B) :
        return "MISSING" # No ROS/ sales data available, IF DO THIS CAN REMOVE FILLNA FROM BEFORE
    else:
        return "C"


def get_grading_percentage(grade_count,total):
    if total == 0:
        return 0
    else:
        return grade_count / total


def get_country_level_order_grading(order, sales,thres=0.1):
    if order > (sales * (1+thres)) :
        return "OVERBUY"
    if order < (sales * (1-thres)):
        return "UNDERBUY"
    else:
        return "APPROVED"


def is_article_size_approved(df,sbatch,articles):
    is_article_size_approved=[]
    for x,y in df.loc[:,['newart','Size']].itertuples(index=False):
        batch = str(x)+str(y)

        if batch in sbatch:
            is_article_size_approved.append("APPROVED") # Match in Size and Art no!
        elif x in articles:
            is_article_size_approved.append("REJECTED")  # Wrong size code used
        else:
            is_article_size_approved.append("UnChecked")

    return is_article_size_approved


def is_article_colour_approved(df,sbatch,articles):
    is_article_colour_approved=[]
    for x,y in df.loc[:,['newart','Colour']].itertuples(index=False):
        batch = str(x)+str(y)

        if batch in sbatch:
            is_article_colour_approved.append("APPROVED") # Match in Size and Art no!
        elif x in articles:
            is_article_colour_approved.append("REJECTED")  # Wrong size code used
        else:
            is_article_colour_approved.append("UnChecked")

    return is_article_colour_approved


def is_article_location_approved(df,conditions):
    is_article_location_approved=[]
    for condition,location in df.loc[:,['Condition','StoreLocation']].itertuples(index=False):
         if condition in conditions:
            if location>1:
                is_article_location_approved.append("REJECTED")
            else:
                is_article_location_approved.append("APPROVED")
         else:
             is_article_location_approved.append("UnChecked")

    return is_article_location_approved




