import os


class Rules:
    def __init__(self):
        self.basepath="/Users/vignesh/Desktop/OrderSummaryDF"

    def get_business_rule_path(self):
        business_rules_path=os.path.join(self.basepath,"business_rules.xlsx")
        return business_rules_path

    def get_article_list_path(self):
        article_list_path=os.path.join(self.basepath,"finalarticlenumber.xlsx")
        return article_list_path

    def get_all_combined_order_summary(self):
        article_list_path=os.path.join(self.basepath,"Combined OS Summer 19.xlsx")
        return article_list_path

    def get_item_master_path(self):
        item_master_path=os.path.join(self.basepath,"Summer19_Combined_Masterfiles.csv")
        return item_master_path

    def get_bill_to_store_list_path(self):
        bill_to_store_list_path=os.path.join(self.basepath,"Ship-To-Bill-To Template - 28 Nov 2018.xlsx")
        return bill_to_store_list_path


    def final_summer_combined_os_path(self):
        final_summer_combined_os_path=os.path.join(self.basepath,"Summer19_Combined_OS_Checked.xlsx")
        return final_summer_combined_os_path


    def final_ros_depth_path(self):
        final_ros_depth_path=os.path.join(self.basepath,"Summer19_Combined_OS_DepthCheck.xlsx")
        return final_ros_depth_path


    def final_otb_os_path(self):
        final_otb_os_path=os.path.join(self.basepath,"Summer19_Combined_OS_OTBCheck.xlsx")
        return final_otb_os_path


    def get_return_on_sales_path(self):
        return_on_sales_path=os.path.join(self.basepath,"CKI 4,8,13 ROS.xls")
        return return_on_sales_path

    def get_franchise_list_path(self):
        franchise_list_data=os.path.join(self.basepath,"Summer 19 FOB by Customer.xlsx")
        return   franchise_list_data

    def get_retail_sales_target_path(self):
        retail_sales_target_path=os.path.join(self.basepath,"Retail Sales Target - FY2019_Revised ASP_Category Contribution.xlsx")
        return retail_sales_target_path







